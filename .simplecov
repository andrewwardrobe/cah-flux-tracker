require 'simplecov'

SimpleCov.start 'rails' do
  add_filter %r{/spec/}
end