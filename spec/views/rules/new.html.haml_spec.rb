# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'rules/new', type: :view do
  before(:each) do
    assign(:rule, create(:rule))
  end

  it 'renders new rule form' do
    render

    assert_select 'form' do
      assert_select 'input[name=?]', 'rule[title]'

      assert_select 'input[name=?]', 'rule[description]'

      assert_select 'input[name=?]', 'rule[group]'

      assert_select 'input[name=?]', 'rule[type]'

      assert_select 'input[name=?]', 'rule[entry_actions]'

      assert_select 'input[name=?]', 'rule[exit_actions]'
    end
  end
end
