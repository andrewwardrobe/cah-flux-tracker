# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'rules/edit', type: :view do
  before(:each) do
    @rule = assign(:rule, create(:rule))
  end

  it 'renders the edit rule form' do
    render

    assert_select 'form[action=?]', rule_path(@rule) do
      assert_select 'input[name=?]', 'rule[title]'

      assert_select 'input[name=?]', 'rule[description]'

      assert_select 'input[name=?]', 'rule[group]'

      assert_select 'input[name=?]', 'rule[type]'

      assert_select 'input[name=?]', 'rule[entry_actions]'

      assert_select 'input[name=?]', 'rule[exit_actions]'
    end
  end
end
