# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'rules/show', type: :view do
  before(:each) do
    @rule = assign(:rule, create(:rule))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Rule Title/)
    expect(rendered).to match(/Rule Description/)
    expect(rendered).to match(/Rule Group/)
    expect(rendered).to match(/house/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
