# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'rules/index', type: :view do
  before(:each) do
    assign(:rules, create_list(:rule, 2))
  end

  it 'renders a list of rules' do
    render
    assert_select 'tr>td', text: 'Rule Title'.to_s, count: 2
    assert_select 'tr>td', text: 'Rule Description'.to_s, count: 2
    assert_select 'tr>td', text: 'Rule Group'.to_s, count: 2
    assert_select 'tr>td', text: 'house'.to_s, count: 2
  end
end
