# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'games/edit', type: :view do
  before(:each) do
    @game = create(:game)
  end

  it 'renders the edit game form' do
    render

    assert_select 'form[action=?][accept-charset=UTF-8][method=?]', game_path(@game), 'post' do
      assert_select 'input[name=?]', 'game[name]'

      assert_select 'input[name=?]', 'game[handsize]'

      assert_select 'input[name=?]', 'game[drawsize]'

      assert_select 'input[name=?]', 'game[rules]'

      assert_select 'input[name=?]', 'game[goals]'
    end
  end
end
