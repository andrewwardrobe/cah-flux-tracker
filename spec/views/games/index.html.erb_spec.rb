# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'games/index', type: :view do
  before(:each) do
    assign(:games, create_list(:game, 2))
  end

  it 'renders a list of games' do
    render
    assert_select 'tr>td', text: 'Name'.to_s, count: 2
    assert_select 'tr>td', text: '10'.to_s, count: 2
    assert_select 'tr>td', text: '1'.to_s, count: 2
  end
end
