# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'games/new', type: :view do
  before(:each) do
    assign(:game, create(:game))
  end

  it 'renders new game form' do
    render

    assert_select 'form' do
      assert_select 'input[name=?]', 'game[name]'

      assert_select 'input[name=?]', 'game[handsize]'

      assert_select 'input[name=?]', 'game[drawsize]'

      assert_select 'input[name=?]', 'game[rules]'

      assert_select 'input[name=?]', 'game[goals]'
    end
  end
end
