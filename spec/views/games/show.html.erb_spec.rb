# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'games/show', type: :view do
  before(:each) do
    @game = create(:game)
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
