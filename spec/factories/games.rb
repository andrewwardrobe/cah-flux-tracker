# frozen_string_literal: true

FactoryBot.define do
  factory :game do
    name 'Name'
    handsize 10
    drawsize 1
    rules []
    goals ['Collect 8 Black cards']
  end
end
