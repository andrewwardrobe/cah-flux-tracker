# frozen_string_literal: true

FactoryBot.define do
  factory :rule do
    title 'Rule Title'
    description 'Rule Description'
    group 'Rule Group'
    type 'house'
    entry_actions {}
    exit_actions {}
  end
end
