# frozen_string_literal: true

class Game
  include Mongoid::Document
  field :name, type: String
  field :handsize, type: Integer
  field :drawsize, type: Integer
  field :rules, type: Array, default: []
  field :goals, type: Array, default: []

  validates :drawsize, numericality: { only_integer: true }
  validates :handsize, numericality: { only_integer: true }
end
