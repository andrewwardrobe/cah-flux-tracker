# frozen_string_literal: true

class Rule
  include Mongoid::Document
  field :title, type: String
  field :description, type: String
  field :group, type: String
  field :type, type: String
  field :entry_actions, type: Hash
  field :exit_actions, type: Hash

  validates :title, presence: true
  validates :description, presence: true
  validates :group, presence: true
  validates :type, format: { with: /(house|starter|normal)/ }, presence: true
end
