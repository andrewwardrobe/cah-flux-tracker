# frozen_string_literal: true

json.extract! rule, :id, :title, :description, :group, :type, :entry_actions, :exit_actions, :created_at, :updated_at
json.url rule_url(rule, format: :json)
